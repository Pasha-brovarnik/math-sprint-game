// Pages
const gamePage = document.getElementById('game-page');
const scorePage = document.getElementById('score-page');
const splashPage = document.getElementById('splash-page');
const countdownPage = document.getElementById('countdown-page');
// Splash Page
const startForm = document.getElementById('start-form');
const radioContainers = document.querySelectorAll('.radio-container');
const radioInputs = document.querySelectorAll('input');
const bestScores = document.querySelectorAll('.best-score-value');
const submitBtn = document.querySelector('.start');
// Countdown Page
const countdown = document.querySelector('.countdown');
// Game Page
const itemContainer = document.querySelector('.item-container');
// Score Page
const finalTimeEl = document.querySelector('.final-time');
const baseTimeEl = document.querySelector('.base-time');
const penaltyTimeEl = document.querySelector('.penalty-time');
const playAgainBtn = document.querySelector('.play-again');

// Equations
let questionAmount = 0;
let equationsArray = [];
let playerGuessArray = [];
let bestScoreArray = [];

// Game Page
let firstNumber = 0;
let secondNumber = 0;
let equationObject = {};
const wrongFormat = [];

// Time
let timer;
let timePlayed = 0;
let baseTime = 0;
let penaltyTime = 0;
let finalTime = 0;
let finalTimeDisplay = '0.0';

// Scroll
let valueY = 0;

// Refrest Splash page best scores
function bestScoresToDOM() {
	bestScores.forEach((score, i) => {
		score.textContent = `${bestScoreArray[i].bestScore}s`;
	});
}

// Check localStorage for best scores
function getSavedBestScores() {
	if (localStorage.getItem('bestScores')) {
		bestScoreArray = JSON.parse(localStorage.bestScores);
	} else {
		bestScoreArray = [
			{ questions: 10, bestScore: finalTimeDisplay },
			{ questions: 25, bestScore: finalTimeDisplay },
			{ questions: 50, bestScore: finalTimeDisplay },
			{ questions: 99, bestScore: finalTimeDisplay },
		];
		localStorage.setItem('bestScores', JSON.stringify(bestScoreArray));
	}
	bestScoresToDOM();
}

// Update best scores array
function updateBestScore() {
	bestScoreArray.forEach((score, i) => {
		if (questionAmount === score.questions) {
			const savedBestScore = Number(bestScoreArray[i].bestScore);
			// Update only if new final score is less or replacing zero
			if (savedBestScore === 0 || savedBestScore > finalTime) {
				bestScoreArray[i].bestScore = finalTimeDisplay;
			}
		}
	});
	// Update Splash page
	bestScoresToDOM();
	// Save to localStorage
	localStorage.setItem('bestScores', JSON.stringify(bestScoreArray));
}

// Reset game
function playAgain() {
	gamePage.addEventListener('click', startTimer);
	scorePage.hidden = true;
	splashPage.hidden = false;
	equationsArray = [];
	playerGuessArray = [];
	valueY = 0;
	playAgainBtn.hidden = true;
	radioContainers.forEach((radioElm) => {
		// Remove selected label styling
		radioElm.classList.remove('selected-label');
	});
}

// Show score page
function showScorePage() {
	// Show play again button after 1 sec
	setTimeout(() => {
		playAgainBtn.hidden = false;
	}, 1000);
	gamePage.hidden = true;
	scorePage.hidden = false;
}

// Format & display time in DOM
function scoresToDOM() {
	finalTimeDisplay = finalTime.toFixed(1);
	baseTime = timePlayed.toFixed(1);
	penaltyTime = penaltyTime.toFixed(1);
	baseTimeEl.textContent = `Base time: ${baseTime}s`;
	penaltyTimeEl.textContent = `Penalty: +${penaltyTime}s`;
	finalTimeEl.textContent = `${finalTimeDisplay}s`;
	updateBestScore();
	// Scroll to top and go to scroll page
	itemContainer.scrollTo({ top: 0, behavior: 'instant' });
	showScorePage();
}

// Stop timer, process results, go to Score page
function checkTime() {
	if (playerGuessArray.length === questionAmount) {
		clearInterval(timer);
		// Check for wrong guess and add penatly time
		equationsArray.forEach((equation, i) => {
			if (equation.evaluated !== playerGuessArray[i]) {
				penaltyTime += 0.5;
			}
		});
		finalTime = timePlayed + penaltyTime;

		scoresToDOM();
	}
}

// Add a tenth of a second to timePlayed
function addTime() {
	timePlayed += 0.1;
	checkTime();
}

// Start timer when game page is clicked
function startTimer() {
	// Rest times
	timePlayed = 0;
	penaltyTime = 0;
	finalTime = 0;
	timer = setInterval(addTime, 100);
	gamePage.removeEventListener('click', startTimer);
}

// Scroll, store user selection
function select(guessedTrue) {
	// Scroll 80 pixels
	valueY += 80;
	itemContainer.scroll(0, valueY);
	// Add player guesses
	return guessedTrue
		? playerGuessArray.push(true)
		: playerGuessArray.push(false);
}

// Display Game page
function showGamePage() {
	gamePage.hidden = false;
	countdownPage.hidden = true;
}

// Get random number up to a max number
function getRandomInt(max) {
	return Math.floor(Math.random() * Math.max(max));
}

// Create Correct/Incorrect Random Equations
function createEquations() {
	// Randomly choose how many correct equations there should be
	const correctEquations = getRandomInt(questionAmount);

	// Set amount of wrong equations
	const wrongEquations = questionAmount - correctEquations;

	// Loop through, multiply random numbers up to 9, push to array
	for (let i = 0; i < correctEquations; i++) {
		firstNumber = getRandomInt(9);
		secondNumber = getRandomInt(9);
		const equationValue = firstNumber * secondNumber;
		const equation = `${firstNumber} x ${secondNumber} = ${equationValue}`;
		equationObject = { value: equation, evaluated: true };
		equationsArray.push(equationObject);
	}
	// Loop through, mess with the equation results, push to array
	for (let i = 0; i < wrongEquations; i++) {
		firstNumber = getRandomInt(9);
		secondNumber = getRandomInt(9);
		const equationValue = firstNumber * secondNumber;
		wrongFormat[0] = `${firstNumber} x ${secondNumber + 1} = ${equationValue}`;
		wrongFormat[1] = `${firstNumber} x ${secondNumber} = ${equationValue - 1}`;
		wrongFormat[2] = `${firstNumber + 1} x ${secondNumber} = ${equationValue}`;
		const formatChoice = getRandomInt(3);
		const equation = wrongFormat[formatChoice];
		equationObject = { value: equation, evaluated: false };
		equationsArray.push(equationObject);
	}

	shuffle(equationsArray);
}

// Add equations to DOM
function equationsToDOM() {
	equationsArray.forEach(({ value }) => {
		// Item
		const item = document.createElement('div');
		item.classList.add('item');
		// Equation text
		const equationText = document.createElement('h1');
		equationText.textContent = value;
		// Append
		item.appendChild(equationText);
		itemContainer.appendChild(item);
	});
}

// Dynamically adding correct/incorrect equations
function populateGamePage() {
	// Reset DOM, Set Blank Space Above
	itemContainer.textContent = '';
	// Spacer
	const topSpacer = document.createElement('div');
	topSpacer.classList.add('height-240');
	// Selected Item
	const selectedItem = document.createElement('div');
	selectedItem.classList.add('selected-item');
	// Append
	itemContainer.append(topSpacer, selectedItem);

	// Create Equations, Build Elements in DOM
	createEquations();
	equationsToDOM();

	// Set Blank Space Below
	const bottomSpacer = document.createElement('div');
	bottomSpacer.classList.add('height-500');
	itemContainer.appendChild(bottomSpacer);
}

// Displays countdown timer
function countdownStart() {
	let count = 3;
	countdown.textContent = count;
	const timeCountdown = setInterval(() => {
		count--;
		if (count === 0) {
			countdown.textContent = 'GO!';
		} else if (count === -1) {
			showGamePage();
			clearInterval(timeCountdown);
		} else {
			countdown.textContent = count;
		}
	}, 1000);
}

// Show Countdown page
function showCountdown() {
	countdownPage.hidden = false;
	splashPage.hidden = true;

	populateGamePage();
	countdownStart();
}

// Get the value from selected radio button
function getRadioValue() {
	let radioValue;
	radioInputs.forEach((radioInput) => {
		if (radioInput.checked) {
			radioValue = radioInput.value;
		}
	});
	return +radioValue;
}

// Form that decides amount of questions
function selectQuestionAmount(e) {
	e.preventDefault();
	questionAmount = getRadioValue();

	if (questionAmount) {
		showCountdown();
		submitBtn.hidden = true;
	}
}

// Event listeners
startForm.addEventListener('click', () => {
	radioContainers.forEach((radioElm) => {
		// Remove selected label styling
		radioElm.classList.remove('selected-label');
		// Add back selected label styling
		if (radioElm.children[1].checked) {
			radioElm.classList.add('selected-label');
			submitBtn.hidden = false;
		}
	});
});

startForm.addEventListener('submit', selectQuestionAmount);
gamePage.addEventListener('click', startTimer);

// On load
getSavedBestScores();
